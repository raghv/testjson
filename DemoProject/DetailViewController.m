//
//  DetailViewController.m
//  DemoProject
//
//  Created by RMC LTD on 20/04/16.
//  Copyright © 2016 Got. All rights reserved.
//

#import "DetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    _userArray = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"userArray"]];
    for (NSDictionary *keyValue in _userArray){
        NSString *id = [keyValue valueForKey:@"id"];
        if ([id isEqualToString:_storyObject[@"db"]]){
            _userName.text = keyValue[@"username"];
            _aboutLabel.text =keyValue[@"about"];
            _tagLabel.text = [NSString stringWithFormat:@"%@ \n followers :%@ ,following :%@",keyValue[@"handle"],keyValue[@"followers"],keyValue[@"following"]];
            [self.userImageView sd_setImageWithURL:[NSURL URLWithString:keyValue[@"image"]] placeholderImage:[UIImage imageNamed:@"minions.jpeg"]];
           BOOL value = [[keyValue objectForKey:@"is_following"] boolValue];
            if (value == NO ){
                [self.followButton  setTitle:@"Follow"
                                    forState:UIControlStateNormal];
                
            }
            else {
                [self.followButton  setTitle:@"Followed"
                                    forState:UIControlStateNormal];
                
            }
             }
    }
    [self.storyImageView sd_setImageWithURL:[NSURL URLWithString:_storyObject[@"si"]]
                          placeholderImage:[UIImage imageNamed:@"minions.jpeg"]];
    _descriptionLabel.text=[NSString stringWithFormat:@"Title :%@ \n %@", _storyObject[@"title"] ,_storyObject[@"description"]];
    [_likeButton setTitle:[NSString stringWithFormat:@"%@", _storyObject[@"likes_count"]] forState:UIControlStateNormal];
    [_commentButton setTitle:[NSString stringWithFormat:@"%@", _storyObject[@"comment_count"]] forState:UIControlStateNormal];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) createUI{
    CGRect screenBound=[[UIScreen mainScreen]bounds];
    self.scrollView =[[UIScrollView alloc]initWithFrame:CGRectMake(0,0,screenBound.size.width,screenBound.size.height)];
    self.scrollView.showsVerticalScrollIndicator=YES;
    self.scrollView.scrollEnabled=YES;
    self.scrollView.userInteractionEnabled=YES;
    self.scrollView.backgroundColor = [UIColor whiteColor];
        CGPoint startPoint = CGPointMake(30, 20);
    self.userImageView = [[UIImageView alloc] initWithFrame:CGRectMake(startPoint.x, startPoint.y, 100, 100)];
    self.userImageView.clipsToBounds = YES;
    [self.userImageView setContentMode:UIViewContentModeScaleAspectFill];
    [self.scrollView addSubview:self.userImageView];
    
    self.userName= [[UILabel alloc] initWithFrame:CGRectMake(startPoint.x + 120, startPoint.y,screenBound.size.width - 180 ,40)];
    
    self.userName.font = [UIFont boldSystemFontOfSize:14.0];
    self.userName.numberOfLines = 1;
    self.userName.backgroundColor = [UIColor clearColor];
    [self.scrollView addSubview:self.userName];
    startPoint.y += 50;
    self.aboutLabel= [[UILabel alloc] initWithFrame:CGRectMake(startPoint.x + 120, startPoint.y,screenBound.size.width - 180,60)];
    
    self.aboutLabel.font = [UIFont boldSystemFontOfSize:11.0];
    self.aboutLabel.numberOfLines = 4;
    self.aboutLabel.backgroundColor = [UIColor clearColor];
    [self.scrollView addSubview:self.aboutLabel];
    startPoint.y += _aboutLabel.frame.size.height + 10;
    self.tagLabel= [[UILabel alloc] initWithFrame:CGRectMake(startPoint.x, startPoint.y,screenBound.size.width - 60,50)];
    
    self.tagLabel.font = [UIFont boldSystemFontOfSize:13.0];
    self.tagLabel.numberOfLines = 2;
    self.tagLabel.backgroundColor = [UIColor clearColor];
    [self.scrollView addSubview:self.tagLabel];
    startPoint.y += _tagLabel.frame.size.height + 10;
    
    self.storyImageView = [[UIImageView alloc] initWithFrame:CGRectMake(30, startPoint.y, screenBound.size.width - 60, 200)];
    self.storyImageView.clipsToBounds = YES;
    [self.storyImageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.scrollView addSubview:self.storyImageView];
    startPoint.y += _storyImageView.frame.size.height + 10;
    
    self.followButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.followButton.frame = CGRectMake(screenBound.size.width /2 - 30 ,  startPoint.y, 100, 25.0);
    [self.followButton  setTitle:@"Follow"
                        forState:UIControlStateNormal];
    [self.followButton addTarget:self action:@selector(followAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:self.followButton];
    startPoint.y += _followButton.frame.size.height + 10;
    self.descriptionLabel= [[UILabel alloc] initWithFrame:CGRectMake(startPoint.x, startPoint.y,screenBound.size.width - 100 , 140)];
    
    self.descriptionLabel.font = [UIFont boldSystemFontOfSize:12.0];
    self.descriptionLabel.numberOfLines = 0;
    
    [self.descriptionLabel minimumScaleFactor];
    self.descriptionLabel.backgroundColor = [UIColor clearColor];
    [self.scrollView addSubview:self.descriptionLabel];
    
    startPoint.y += _descriptionLabel.frame.size.height + 10;
    
    
    self.likeButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.likeButton.frame = CGRectMake(startPoint.x, startPoint.y, 100, 40);
    [self.likeButton   setImage:[UIImage imageNamed:@"ic_thumb_up_18pt"]
                       forState:UIControlStateNormal];
    [self.scrollView addSubview:self.likeButton];
    startPoint.x += _likeButton.frame.size.width + 10;
    self.commentButton = [UIButton buttonWithType:UIButtonTypeSystem];
    
    self.commentButton.frame = CGRectMake(startPoint.x, startPoint.y, 100, 40);
    [self.commentButton  setImage:[UIImage imageNamed:@"ic_comment_18pt"]
                         forState:UIControlStateNormal];
    [self.scrollView addSubview:self.commentButton];
    
    startPoint.y += _commentButton.frame.size.height + 10;
    // Do any additional setup after loading the view.
    [self.view addSubview:_scrollView];
    NSLog(@"%f",startPoint.y);
    self.scrollView.contentSize = CGSizeMake(screenBound.size.width, startPoint.y);
    
}
-(void)followAction :(UIButton *) sender{
    NSMutableDictionary *newDict;
    NSUInteger index = 0;
    for (NSMutableDictionary *keyValue in _userArray){
        NSString *id = [keyValue valueForKey:@"id"];
        if ([id isEqualToString:_storyObject[@"db"]]){
            
            BOOL value = [[keyValue objectForKey:@"is_following"] boolValue];
            
            newDict = [keyValue mutableCopy];
            if (value == YES) {
                [newDict setObject:[NSNumber numberWithBool:NO]  forKey:@"is_following"];
                [self.followButton  setTitle:@"Follow"
                                    forState:UIControlStateNormal];
            }
            else {
                [newDict setObject:[NSNumber numberWithBool:YES]  forKey:@"is_following"];
                [self.followButton  setTitle:@"Followed"
                                    forState:UIControlStateNormal];
            }
           
            
            index = [_userArray indexOfObject:keyValue];
            
       //[_userArray replaceObjectAtIndex:index withObject:newDict];
            //[keyValue setObject:[NSNumber numberWithBool:value] forKey:@"is_following"];
            //[keyValue setValue: [NSNumber numberWithBool:value] forKey:@"is_following"];
        }
    }
    [_userArray removeObjectAtIndex:index];
    [_userArray addObject:newDict];
    [[NSUserDefaults standardUserDefaults] setObject:_userArray forKey:@"userArray"];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
