//
//  StoryTableViewCell.m
//  DemoProject
//
//  Created by RMC LTD on 18/04/16.
//  Copyright © 2016 Got. All rights reserved.
//

#import "StoryTableViewCell.h"

@implementation StoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // configure control(s)
        
        CGRect screenBound=[[UIScreen mainScreen]bounds];

        CGRect myFrame = CGRectMake(80, 5, screenBound.size.width - 150 , 30);
        self.titleLabel= [[UILabel alloc] initWithFrame:CGRectMake(myFrame.origin.x , 5.0, myFrame.size.width, 40)];
        
        self.titleLabel.font = [UIFont boldSystemFontOfSize:14.0];
        self.titleLabel.numberOfLines = 2;
        self.titleLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:self.titleLabel];
       self.storyImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5
                                                                    , 60, 60)];
        //self.storyImageView.clipsToBounds = YES;
        [self.storyImageView setContentMode:UIViewContentModeScaleAspectFill];
        [self addSubview:self.storyImageView];
        self.followButton = [UIButton buttonWithType:UIButtonTypeSystem];
        self.followButton.frame = CGRectMake(myFrame.origin.x + myFrame.size.width -30, 5.0, 100, 25.0);
        [self.followButton  setTitle:@"Follow"
                            forState:UIControlStateNormal];
        [self addSubview:self.followButton];
        myFrame.origin.y += 30;
        self.descriptionLabel= [[UILabel alloc] initWithFrame:CGRectMake(myFrame.origin.x, myFrame.origin.y,screenBound.size.width - 100 , 120)];
        
        self.descriptionLabel.font = [UIFont boldSystemFontOfSize:12.0];
        self.descriptionLabel.numberOfLines = 0;
        
        [self.descriptionLabel minimumScaleFactor];
        self.descriptionLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:self.descriptionLabel];
        
        

        myFrame.origin.y += 110;
        self.LikeButton = [UIButton buttonWithType:UIButtonTypeSystem];
        self.LikeButton.frame = CGRectMake(myFrame.origin.x, myFrame.origin.y, 100, 40);
        [self.LikeButton   setImage:[UIImage imageNamed:@"ic_thumb_up_18pt"]
                            forState:UIControlStateNormal];
        [self addSubview:self.LikeButton];
        self.commentButton = [UIButton buttonWithType:UIButtonTypeSystem];
        myFrame.origin.x += 115;
        self.commentButton.frame = CGRectMake(myFrame.origin.x, myFrame.origin.y, 100, 40);
        [self.commentButton  setImage:[UIImage imageNamed:@"ic_comment_18pt"]
                            forState:UIControlStateNormal];
        [self addSubview:self.commentButton];
        
       
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
