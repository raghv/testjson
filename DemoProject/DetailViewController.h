//
//  DetailViewController.h
//  DemoProject
//
//  Created by RMC LTD on 20/04/16.
//  Copyright © 2016 Got. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController
@property (nonatomic, strong) UIImageView *storyImageView;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIImageView *panelView;
@property (nonatomic, strong) UIImageView *userImageView;
@property (nonatomic, strong) UILabel *descriptionLabel;
@property (nonatomic, strong) UILabel *tagLabel;
@property (nonatomic, strong) UILabel *userName;
@property (nonatomic, strong) UILabel *aboutLabel;
@property (nonatomic, strong) UIButton *followButton;
@property (nonatomic, strong) UIButton *likeButton;
@property (nonatomic, strong) UIButton *commentButton;
@property (nonatomic, strong) NSMutableArray *userArray;
@property (nonatomic, strong) id storyObject;
@end
