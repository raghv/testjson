//
//  StoryTableViewCell.h
//  DemoProject
//
//  Created by RMC LTD on 18/04/16.
//  Copyright © 2016 Got. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface StoryTableViewCell : UITableViewCell
@property (nonatomic, strong) UILabel *descriptionLabel;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *storyImageView;
@property (nonatomic, strong) UIButton *followButton;
@property (nonatomic, strong) UIButton *LikeButton;
@property (nonatomic, strong) UIButton *commentButton;
@end
