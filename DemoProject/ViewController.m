//
//  ViewController.m
//  DemoProject
//
//  Created by RMC LTD on 18/04/16.
//  Copyright © 2016 Got. All rights reserved.
//

#import "ViewController.h"
#import "StoryTableViewCell.h"
#import "DetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface ViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property  NSArray* dataList;
@property  NSMutableArray* userList;
@property  NSMutableArray* storyList;

@end

@implementation ViewController

- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = YES;
    self.userList = [NSMutableArray new];
    self.storyList = [NSMutableArray new];

    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds];
    
    // Make ourselves the datasource (and delegate)
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    // Let the tableview know how to make cells
    //[self.tableView registerClass:[UITableViewCell class]
          // forCellReuseIdentifier:@"cell"];
    
    // Add the tableview to the screen
    [self.view addSubview:self.tableView];
    [self readDataFromFile];
    
    


    // Do any additional setup after loading the view, typically from a nib.
}
-(void)readDataFromFile
{
    NSString * filePath =[[NSBundle mainBundle] pathForResource:@"iOS-Android Data" ofType:@"json"];
    
    NSError * error;
    NSString* fileContents =[NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
    
    
    if(error)
    {
        NSLog(@"Error reading file: %@",error.localizedDescription);
    }
    
    
    self.dataList = (NSArray *)[NSJSONSerialization
                                JSONObjectWithData:[fileContents dataUsingEncoding:NSUTF8StringEncoding]
                                options:0 error:NULL];
    for ( NSDictionary *data in _dataList){
        id keyValuePair = data;
        if (keyValuePair[@"username"]){
            [_userList addObject:data];
    }
        else {
            [_storyList addObject:data];
        }
}
    
    [[NSUserDefaults standardUserDefaults] setObject:_userList forKey:@"userArray"];

    [self.tableView reloadData];

    
}



-(void) viewDidAppear:(BOOL)animated {
    _userList = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"userArray"]];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 180;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.storyList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Similar to UITableViewCell, but
    StoryTableViewCell *cell = (StoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[StoryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    id keyValuePair =self.storyList[indexPath.row];

    
        cell.titleLabel.text = keyValuePair[@"title"];
    
//    if ([keyValuePair[@"description"] isEqualToString:@""]){
//        cell.descriptionLabel.text=@"No Description";
//        
//    }
//    else {
        cell.descriptionLabel.text=[NSString stringWithFormat:@"%@", keyValuePair[@"description"]];
    //}
   // keyValuePair[@"likes_count"]
    //keyValuePair[@"comment_count"]si
    [cell.storyImageView sd_setImageWithURL:[NSURL URLWithString:keyValuePair[@"si"]]
                           placeholderImage:[UIImage imageNamed:@"minions.jpeg"]
options:SDWebImageRefreshCached]; 
    [cell.LikeButton setTitle:[NSString stringWithFormat:@"%@", keyValuePair[@"likes_count"]] forState:UIControlStateNormal];
    [cell.commentButton setTitle:[NSString stringWithFormat:@"%@", keyValuePair[@"comment_count"]] forState:UIControlStateNormal];
    
    
    for (NSDictionary *userData in _userList){
        NSString *id = [userData valueForKey:@"id"];
        if ([id isEqualToString:keyValuePair[@"db"]]){
            BOOL value = [[userData objectForKey:@"is_following"] boolValue];
            if (value == YES){
                [cell.followButton  setTitle:@"Followed"
                                    forState:UIControlStateNormal];
                
            }
            else {
                [cell.followButton  setTitle:@"Follow"
                                    forState:UIControlStateNormal];
                
            }
            
        }
    }
    //}
    
    
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    DetailViewController *detailVC=[[DetailViewController alloc] init];
    //detailVC.userArray = _userList;
    detailVC.storyObject = _storyList[indexPath.row];
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
