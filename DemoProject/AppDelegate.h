//
//  AppDelegate.h
//  DemoProject
//
//  Created by RMC LTD on 18/04/16.
//  Copyright © 2016 Got. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

